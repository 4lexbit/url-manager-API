from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

v1 = [
    path('v1/', include('authentication.urls')),
    path('v1/', include('urlman.urls', namespace='urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(v1)),
]
