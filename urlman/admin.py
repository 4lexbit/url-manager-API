from django.contrib import admin

from .models import UrlItem, UrlViews


@admin.register(UrlItem)
class UrlItemAdmin(admin.ModelAdmin):
    list_display = ('owner', 'entered_url', 'short_code', 'creation_date')


@admin.register(UrlViews)
class UrlViewsAdmin(admin.ModelAdmin):
    list_display = ('item', 'ip', 'view_date')
