from django.urls import path

from .views import UrlList, ClientRedirection, UrlSingle, UrlViewDelete

app_name = 'urls'

urlpatterns = [
    path('urls/', UrlList.as_view(), name='list'),
    path('urls/<str:short_code>/', UrlSingle.as_view(), name='detail'),
    path('views/<int:pk>/', UrlViewDelete.as_view(), name='delete_view'),
    path('<str:short_code>/', ClientRedirection.as_view(), name='view'),
]
