from rest_framework.relations import HyperlinkedIdentityField
from rest_framework.serializers import HyperlinkedModelSerializer, ModelSerializer

from urlman.models import UrlItem, UrlViews


class UrlViewsSerializer(ModelSerializer):
    class Meta:
        model = UrlViews
        fields = ('id', 'ip', 'view_date')


class UrlItemListSerializer(ModelSerializer):
    class Meta:
        model = UrlItem
        fields = ('id', 'entered_url', 'short_code', 'creation_date')


class UrlItemDetailSerializer(ModelSerializer):
    views = UrlViewsSerializer(UrlViews, many=True)

    class Meta:
        model = UrlItem
        fields = ('id', 'entered_url', 'short_code', 'creation_date', 'views')
