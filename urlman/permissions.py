from rest_framework.permissions import BasePermission


class IsOwner(BasePermission):

    def has_permission(self, request, view):
        return True

    def has_object_permission(self, request, view, obj):
        # return (obj.owner == request.user) or (obj.item.owner == request.user)
        if hasattr(obj, 'owner'):
            return (obj.owner == request.user)
        else:
            return (obj.item.owner == request.user)
