from django.db import models

from authentication.models import User
from .utils import generate_short_code, get_client_ip


class UrlItem(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    entered_url = models.URLField()
    short_code = models.CharField(max_length=15, unique=True, null=True, blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-creation_date']
        verbose_name_plural = 'Url Items'

    def save(self, *args, **kwargs):
        if not self.short_code:
            self.short_code = generate_short_code()
            while UrlItem.objects.filter(short_code=self.short_code).exists():
                self.short_code = generate_short_code()
        super(UrlItem, self).save()

    def __str__(self):
        return self.short_code


class UrlViews(models.Model):
    item = models.ForeignKey(UrlItem, on_delete=models.CASCADE, related_name='views')
    ip = models.GenericIPAddressField(null=True, blank=True)
    view_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-view_date']
        verbose_name_plural = 'Url Views'

    def __str__(self):
        return self.ip
