from django.apps import AppConfig


class UrlmanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'urlman'
