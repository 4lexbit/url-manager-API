from django.shortcuts import get_object_or_404
from django.views.generic import RedirectView
from rest_framework import status

from rest_framework.generics import RetrieveDestroyAPIView, ListCreateAPIView, DestroyAPIView

from ipware import get_client_ip
from rest_framework.response import Response

from authentication.models import User
from .models import UrlItem, UrlViews
from .permissions import IsOwner
from .serializers import UrlItemListSerializer, UrlItemDetailSerializer, UrlViewsSerializer


class ClientRedirection(RedirectView):
    permanent = False

    def get_redirect_url(self, *args, **kwargs):
        url = get_object_or_404(UrlItem, short_code=kwargs['short_code'])
        client_ip, is_routable = get_client_ip(self.request)
        UrlViews.objects.create(item=url, ip=client_ip)
        return url.entered_url


class UrlList(ListCreateAPIView):
    serializer_class = UrlItemListSerializer

    permission_classes = (IsOwner,)

    def get_queryset(self):
        return UrlItem.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        owner = get_object_or_404(User, id=self.request.user.id)
        return serializer.save(owner=owner)


class UrlSingle(RetrieveDestroyAPIView):
    serializer_class = UrlItemDetailSerializer
    queryset = UrlItem.objects.all()
    lookup_field = 'short_code'

    permission_classes = (IsOwner,)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response('Successfully deleted', status=status.HTTP_204_NO_CONTENT)


class UrlViewDelete(DestroyAPIView):
    serializer_class = UrlViewsSerializer
    queryset = UrlViews.objects.all()
    permission_classes = (IsOwner,)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response('Successfully deleted', status=status.HTTP_204_NO_CONTENT)
