import random
import string


def generate_short_code(length=8):
    return ''.join([random.choice(string.digits + string.ascii_letters) for i in range(0, length)])


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
