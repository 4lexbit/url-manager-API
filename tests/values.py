URL_PATH = 'http://127.0.0.1:8000/api/v1/'

TEST_USER_1 = {
    'email': 'spongebob@krusty.com',
    'password': 'SquarePants',
}

TEST_USER_2 = {
    'email': 'patric_star@krusty.com',
    'password': 'Star22',
}

TEST_URL_1 = {
    'entered_url': 'https://github.com',
    'short_code': 'spongecc',
}

TEST_URL_2 = {
    'entered_url': 'https://instagram.com/',
    'short_code': 'patriccc',
}

TEST_VIEW = {
    'ip': '200.0.0.1',
}
