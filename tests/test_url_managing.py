from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from authentication.models import User
from tests.values import TEST_USER_1, TEST_USER_2, TEST_URL_1, TEST_VIEW, URL_PATH, TEST_URL_2
from urlman.models import UrlItem, UrlViews


class TestURLManaging(APITestCase):

    def setUp(self):
        self.test_user_1 = User.objects.create_user(**TEST_USER_1)
        self.test_user_1.is_active = True
        self.test_user_1.save()

        self.test_user_2 = User.objects.create_user(**TEST_USER_2)
        self.test_user_2.is_active = True
        self.test_user_2.save()

        self.test_url = UrlItem.objects.create(**TEST_URL_1, owner=self.test_user_1)
        self.test_view = UrlViews.objects.create(**TEST_VIEW, item=self.test_url)

    @property
    def bearer_token(self):
        refresh = RefreshToken.for_user(self.test_user_2)
        return {"HTTP_AUTHORIZATION": f'Bearer {refresh.access_token}'}

    def test_redirect(self):
        url = URL_PATH + self.test_url.short_code + '/'
        response = self.client.get(url, **self.bearer_token)
        views = UrlViews.objects.filter(item__owner=self.test_user_1).count()
        self.assertEqual(views, 2)
        self.assertEqual(response.url, self.test_url.entered_url)
        self.assertRedirects(response, self.test_url.entered_url, status_code=status.HTTP_302_FOUND,
                             target_status_code=status.HTTP_200_OK, fetch_redirect_response=False)

    def test_list_create_url(self):
        url = URL_PATH + 'urls/'
        response = self.client.post(url, TEST_URL_2, **self.bearer_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(url, **self.bearer_token)
        self.assertEqual(response.data[0].get('entered_url'), TEST_URL_2.get('entered_url'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve_destroy_single_urls(self):
        url = URL_PATH + 'urls/'
        response = self.client.post(url, TEST_URL_2, **self.bearer_token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        url += response.data.get('short_code') + '/'
        response = self.client.get(url, **self.bearer_token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.delete(url, **self.bearer_token)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
