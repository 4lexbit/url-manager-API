from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from authentication.models import User

from .values import TEST_USER_1, URL_PATH


class TestJWTAuthentication(APITestCase):

    def setUp(self):
        self.test_user = User.objects.create_user(**TEST_USER_1)
        self.test_user.is_active = True
        self.test_user.save()

    def test_auth(self):
        url = URL_PATH + 'token/'
        self.assertEqual(self.test_user.is_active, 1, 'Active User')
        response = self.client.post(url, {'email': TEST_USER_1.get('email'), 'password': TEST_USER_1.get('password')},
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
