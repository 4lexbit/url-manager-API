from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from .views import UserCreate, BlacklistTokenUpdateView

app_name = 'auth'

urlpatterns = [
    path('signup/', UserCreate.as_view(), name='signup'),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('logout/', BlacklistTokenUpdateView.as_view(), name='blacklist'),
]

